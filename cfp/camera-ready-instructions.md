---
layout: page
---


# Camera-Ready Instructions

Camera-ready papers for the MAISP 2021 Proceedings are due **June 14**. In essence, you need to do the following:


## 1. Format paper

- Camera-ready papers should adhere to the [ACM templates](http://www.acm.org/publications/proceedings-template). Templates are provided for formatting papers in MS Word and LaTeX. In LaTeX, you should use `\documentclass[sigconf]{acmart}`.

- Papers should be no more than 6 pages (including references) in 2-column 9pt ACM format. Note that the original submission instructions suggested using the old ACM template with 10pt. Using the new ACM template means that you have more space, to address the comments by the reviewers.

- Please do NOT number the pages of your camera-ready paper (by default page numbers are disabled in the ACM template).

- You should provide proper indexing information in the final version according to the ACM Computing Classification System (CCS). More information about the ACM CCS is available on the [ACM CCS website](https://dl.acm.org/ccs).

- Avoid using any special characters or non-standard fonts. We must be able to display and print your submission exactly as we receive it using only standard tools and printers, so we strongly suggest that you use only standard fonts embedded in the PDF file.


## 2. Rights management form

- The copyright forms will be made available to authors in the HotCRP system when you edit submission.

- After completing the rights management form, you find the correct rights management text and bibliographic strip in the HotCRP - Final version preparation. You MUST place them in your paper.


## 3. Submit camera-ready version

- Go to the MAISP 2021 submission page and edit your paper: [maisp21.hotcrp.com](https://maisp21.hotcrp.com/)

- Fill in all information, submit the source files and PDF of your camera-ready paper.


### Questions
If you have any questions about the camera-ready process, feel free to contact MAISP 2021 publication chair Fan Mo ([f.mo18@imperial.ac.uk](f.mo18@imperial.ac.uk)).
