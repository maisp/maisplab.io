---
layout: page
---

# Call for Papers

[**Submission website is closed!**](https://maisp21.hotcrp.com)
{: .alert .alert-danger}

The **S&P for Mobile AI** workshop aims to bring together researchers in the areas of security and privacy with respect to mobile systems, networking, and AI, to discuss challenging topics, share new ideas, and exchange experiences across these areas, from both theoretical and experimental perspectives. In particular, we are interested in contributions that discuss what type of security and privacy problems (e.g., attacks, leaks, etc.) the mobile AI will raise in the near future, and what kind of algorithmic or practical solutions and adaptations must be employed at the application, operating system or network level to solve these challenges.

We invite submissions of original, previously unpublished papers addressing key challenges in the intersection of the following (tentative) list of topics:

- Secure / privacy-preserving distributed learning (P2P, FL, etc.)
- Secure / privacy-preserving network functions at the edge
- Security / privacy issues in FL systems (e.g., mobile / edge systems)
- Security / privacy issues in FL scheduling algorithms
- Security / privacy issues in on-device training, algorithms, analytics
- Security / privacy issues in Biometrics/Fingerprinting in mobile systems
- Security / privacy issues in mobile medical and health (MHealth) systems
- Security / privacy issues in (Covid) mobile contact tracing technologies
- Security / privacy issues in voice agents and local interactions
- Trusted / attestable mobile / edge systems
- Detection of automated / bot mobile users (e.g., mobile farms)

We invite submissions which can be either full technical workshop papers, or position papers. Maximum length of such submissions is 6 pages (including references) in 2-column 10pt ACM format.

All the submissions should be double-blind and will be peer-reviewed. For anonymity purposes, you must remove any author names and other uniquely identifying features in your submitted paper (e.g., references to your past work, links to data/code, etc).

All submissions must be uploaded to the workshop submission site available here: **[https://maisp21.hotcrp.com](https://maisp21.hotcrp.com)**.

Any questions regarding submission issues should be directed to **nicolas.kourtellis (at) telefonica.com**.


**Workshop Submission Deadline**: May 14th, 2021, 23:59 AOE  \\
**Reviews Deadline**: May 21st, 2021 \\
**Acceptance Notifications**: May 27th, 2021 \\
**Camera Ready Deadline**: June 14th, 2021 \\
**ACM-ready papers**: June 15th, 2021 \\
**Workshop Date**: June 24th, 2021
