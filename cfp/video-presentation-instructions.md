---
layout: page
---

# Video Instructions

Paper presentations are going to be pre-recorded and played during the conference, while Q&A after the presentation will be live. Pre-recorded videos for the MAISP 2021 are due **June 18**. In essence, you need to do the following:


## 1. Release and consent form

- The presenters are requested to sign the audio/video release and consent form ([download here](https://maisp.gitlab.io/cfp/SIGMOBILE_Permission_Form.docx)). Note that, workshop participants to have their videos hosted on ACM SIGMOBILE YouTube channel is voluntary and optional, but we strongly encourage authors to do so to increase the exposure of your work.

- Send the signed form via emails to Publication Chair Fan Mo ([f.mo18@imperial.ac.uk](f.mo18@imperial.ac.uk)).


## 2. Record a video

- The video should be **no longer than 15 minutes**. Videos will be made publicly available on the ACM SIGMOBILE YouTube channel on the first day of the conference.

- The video file MUST meet the following requirements:
    * Landscape orientation 16:9
    * Resolution: 1280 x 720 (=16:9 ratio, 720p)
    * FPS: 30 frames / sec
    * In .MP4 format optimized for streaming
    * The Video Codec MUST be H.264
    * The Audio Codec MUST be AAC
    * Max file size: 2GB

- Please check that the audio level IS NOT too low.

- We recommend you to focus on the key innovation and high-level ideas and to favor intuition over detailed explanations, especially for pre-recorded presentations. Remember that attendees can always look up your paper to find the details.

- You are free to use any video recording/editing software. One option that works across Windows and Mac is the recording feature in Zoom. Zoom allows you to share your screen, turn on your webcam and record your presentation.


## 3. Upload the video

- Name your video files as:
    * \$HOTCRP-PAPER-ID\$_\$LASTNAME-1ST-AUTHOR\$_\$PAPER-TITLE\$.mp4 
    * For example: 23_Davis_OneOfBestMAISPSystem.mp4

- Upload the video to the Google Drive folder: [https://bit.ly/3c8oJhE](https://bit.ly/3c8oJhE)


### Questions
If you have any questions about the video presentation process, feel free to contact MAISP 2021 video chair Fan Mo ([f.mo18@imperial.ac.uk](f.mo18@imperial.ac.uk)).
