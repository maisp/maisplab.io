---
layout: page
---

# Workshop committees

## Steering Committee

[Cristian Canton](https://cristiancanton.github.io) \\
Facebook AI Red Team

[Kleomenis Katevas](https://minoskt.github.io) \\
Telefonica Research

[Nicolas Kourtellis](https://www.linkedin.com/in/nicolas-kourtellis-3a154511/) \\
Telefonica Research

[Ilias Leontiadis](https://leontiadis.net) \\
Samsung AI

## Publication / Video Chair

[Fan (Vincent) Mo](https://mofanv.github.io/) \\
Imperial College London

## TPC Members

[Mario Almeida](https://4knahs.gitlab.io/4knahs/) \\
Samsung AI

[Hassan Asghar](https://researchers.mq.edu.au/en/persons/hassan-asghar) \\
Macquarie University

[Eugene Bagdasaryan](https://ebagdasa.github.io) \\
Cornell Tech

[Mathieu Cunche](http://perso.citi-lab.fr/mcunche/index.html) \\
INSA-Lyon - CITI, Inria

[Soteris Demetriou](https://www.soterisdemetriou.com) \\
Imperial College London

[Eduard Marin Fabregas](https://www.linkedin.com/in/eduard-marin-76a6a026) \\
Telefonica Research

[Muhammad Ikram](https://imikr4m.github.io) \\
Macquarie University

[Stefanos Laskaridis](https://stevelaskaridis.github.io) \\
Samsung AI

[Mohammad Malekzadeh](https://mmalekzadeh.github.io) \\
Imperial College London

[Anna Maria Mandalari](https://www.imperial.ac.uk/people/anna-maria.mandalari) \\
Imperial College London

[Ilya Mironov](https://ai.facebook.com/people/ilya-mironov/) \\
Facebook AI

[Fan (Vincent) Mo](https://mofanv.github.io/) \\
Imperial College London

[Panagiotis Papadopoulos](https://www.linkedin.com/in/panagiotis-panos-papadopoulos-7aa80560/) \\
Telefonica Research

[Cigdem Sengul](https://cigdemsengul.com) \\
Brunel University London

[Ali Shahin Shamsabadi](https://alishahin.github.io) \\
Vector Institute

[Guillermo Suarez-Tangil](https://nms.kcl.ac.uk/guillermo.suarez-tangil/)\\
IMDEA Networks Institute

[Davide Testuggine](https://www.linkedin.com/in/testuggine/) \\
Facebook AI

[Güliz Seray Tuncay](https://www.gulizseray.com) \\
Android Security, Google

[Peter Vajda](https://sites.google.com/site/vajdap/what-we-do?authuser=0) \\
Facebook AI

[Matteo Varvello](https://scholar.google.com/citations?user=xZvdwq0AAAAJ&hl=en) \\
Nokia Bell Labs

[Yuchen Zhao](https://yuchenzhao.github.io) \\
Imperial College London
