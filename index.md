---
layout: home
---
<img src="assets/images/cover.jpg" width="100%">

Updates: Workshop planned for June 24th, 2021
{: .alert .alert-info}

Updates: Deadline extended for May 14th, 23:59, 2021
{: .alert .alert-info}

[**Submission website is closed!**](https://maisp21.hotcrp.com)
{: .alert .alert-danger}

Mobile systems (e.g., mobile phones, wearables, mobile edge devices, etc.) are currently used to perform increasingly complex computation tasks. At the application level, they can facilitate online/offline multiplayer gaming, perform user health and behavioral tracking to provide recommendations to users for diversifying their physical activities, detect changes or anomalies in user behavior for device fingerprinting, etc. All such tasks rely on varying-level of complexity AI models, usually in the form of deep neural networks (DNNs). In the past, such models were trained in a centralized fashion on offline/already collected data and either came pre-installed in the user applications or delivered dynamically to the devices.

Recently, we have seen a shift of this paradigm, with the introduction of distributed AI paradigms such as Federated Learning (FL), Peer-to-Peer Machine Learning (ML), and other variants. These paradigms fit well with the distributed setting that mobile systems have by definition, allowing ML algorithms to scale to millions of devices and user data, while users maintain ownership of their data, i.e., they do not need to upload them to centralized datacenters for storing and analysis. However, they usually require several (distributed/decentralized) rounds of on-device training of said ML models across many participating mobile devices, before any useful inference with said models can be performed by the user applications employing them.

This paradigm shift is accompanied with several challenges and open research questions:
- What type of security and privacy problems (e.g., attacks, leaks, etc.) do the mobile AI training and inference raise?
- How can these new paradigms be adapted and used to strengthen the security and privacy of the user involved, whose data are used within these mobile-based AI models?
- What adaptations must be done at the application, operating system or network level?

This 1st edition of the **S&P for Mobile AI** workshop aims to bring together researchers in the areas of security and privacy with respect to mobile systems, networking, and AI, to discuss challenging topics, share new ideas, and exchange experiences across these areas, from both theoretical and experimental perspectives. We invite submissions of original, previously unpublished papers addressing key challenges in the intersection of these topics.

The workshop is co-located with [ACM MobiSys 2021](https://www.sigmobile.org/mobisys/2021/) in a virtual format (on-line). All accepted papers will also be included in the conference proceedings and be made available in the ACM Digital Library.

<!-- The workshop will take place on the 1st of December 2020. -->

Deadline for paper submissions: **May 14th, 2021 - 23:59 AOE**.

[**Submission website is closed!**](https://maisp21.hotcrp.com)
{: .alert .alert-danger}
