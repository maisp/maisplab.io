---
layout: page
---

[**All times mentioned are in CEST!**]
{: .alert .alert-danger}

**1400-1415: Opening Remarks** \\
*Nicolas Kourtellis (Telefonica Research)*

---

**1415-1515: Keynote 1: Paul Patras -- Can we trust AI to secure our edge?** \\
*Chair: Nicolas Kourtellis (Telefonica Research)*

---

**1515-1615: Paper Session 1** \\
*Chair: Panagiotis Papadopoulos (Telefonica Research)*


1515-1535: Open, Sesame! Introducing Access Control to Voice Services.
*Dominika Woszczyk (Imperial College London), Alvin Lee (Imperial College London), Soteris Demetriou (Imperial College London)*

1535-1555: Your Eyes Show What Your Eyes See (Y-EYES): Challenge-Response Anti-Spoofing Method for Mobile Security Using Corneal Specular Reflections.
*Muhammad Mohzary (School of Computing and Engineering, University of Missouri-Kansas City, MO, USA, Department of Computer Science, Jazan University, Jazan, Saudi Arabia), Khalid J Almalki (School of Computing and Engineering, University of Missouri-Kansas City, MO, USA, College of Computing and Informatics, Saudi Electronic University, Riyadh, Saudi Arabia), Baek-Young Choi (School of Computing and Engineering, University of Missouri-Kansas City, MO, USA), Sejun Song (School of Computing and Engineering, University of Missouri-Kansas City, MO, USA)*

1555-1615: Temporal Consistency Checks to Detect LiDAR Spoofing Attacks on Autonomous Vehicle Perception.
*Chengzeng You (Imperial College London), Zhongyuan Hau (Imperial College London), Soteris Demetriou (Imperial College London)*

---

**1615-1630: Break 1**

---

**1630-1730: Keynote 2: Hamed Haddadi -- Defence Against Dark Artefacts** \\
*Chair: Kleomenis Katevas (Telefonica Research)*

---

**1730-1810: Paper Session 2** \\
*Chair: Stefanos Laskaridis (Samsung AI)*

1730-1750: Surprising Privacy Threats from Innocuous Sensors.
*Shirish Singh (Columbia University)*

1750-1810: Stochastic-Shield: A Probabilistic Approach Towards Training-Free Adversarial Defense in Quantized CNNs.
*Lorena Qendro (University of Cambridge), Sangwon Ha (Arm ML Research Lab), Rene de Jong (Arm ML Research Lab), Partha Maji (Arm ML Research Lab)*

---

**1810-1830: Break 2**

---

**1830-1930: Keynote 3: Diego Perino -- Decentralized Privacy Preserving Artificial Intelligence for Next Generation Telco Industry** \\
*Chair: Ilias Leontiadis (Samsung AI)*

---

**1930-2030: Keynote 4: Nic Lane -- The ML data center is dead: What comes next?** \\
*Chair: Cristian Canton (Facebook AI Red Team)*

---

**2030: Concluding Remarks** \\
*Nicolas Kourtellis (Telefonica Research)*
