---
layout: page
---

### [Paul Patras](https://homepages.inf.ed.ac.uk/ppatras/)
**University of Edinburgh | Net AI**

![image](../assets/images/patras.jpg){: width="250"}

Paul Patras is an Associate Professor in the School of Informatics at the University of Edinburgh, where he leads the Mobile Intelligence Lab. His research crosses the boundaries between mobile networking, security, and data science. His team has pioneered several applications of AI to the analysis, security, and management of mobile systems. Paul is also co-founder and CEO of Net AI, a university spin-out whose mission is to put mobile network management on autopilot.

**Talk:** [Can we trust AI to secure our edge?](/talks/)

---


### [Hamed Haddadi](https://haddadi.github.io)
**Imperial College London | Brave Software**

![image](../assets/images/HamedHaddadi.jpg){: width="250"}

Hamed is a Reader in Human-Centred Systems at Imperial College London. He is also a Visiting Professor at Brave Software where he works on developing privacy-preserving analytics protocols. He enjoys designing and building systems that enable better use of our digital footprint, while respecting users' privacy.

**Talk:** [Defence Against Dark Artefacts](/talks/)

---

### [Diego Perino](http://diegoperino.com)
**Telefonica Research**

![image](../assets/images/perino.jpg){: width="250"}

Diego Perino is the Director of Telefonica Research and lead a team of researchers in Machine Learning/Deep Learning, HCI, Privacy/Security, Networks and Systems. Recently, he also lead ARIANA, a Telefonica Innovation startup project leveraging Artificial Intelligence for Automatic Network Actions.  Previously he worked at Bell Labs (Paris, France) from September 2009 to June 2016 and six months at NICTA (Sydney, Australia) in 2016 as visiting researcher. He received his Ph.D. in Computer Science from the Paris Diderot-Paris 7 university in 2009: the thesis was a collaboration between Orange Labs and INRIA through a CIFRE contract.


**Talk:** [Decentralized Privacy Preserving Artificial Intelligence for Next Generation Telco Industry](/talks/)

---

### [Nicholas Lane](http://niclane.org)
**University of Cambridge | Samsung AI Center**

![image](../assets/images/NicLane.jpg){: width="250"}

Nic Lane is a Senior Lecturer (Associate Professor) in the department of Computer Science and Technology at the University of Cambridge, where he leads the Machine Learning Systems lab ([https://mlsys.cst.cam.ac.uk](https://mlsys.cst.cam.ac.uk)).
Along side his academic role, Nic is also a Director at the Samsung AI Center in Cambridge where his teams focus on on-device and distributed forms of machine learning. Additional details are available from: http://niclane.org/


**Talk:** [The ML data center is dead: What comes next?](/talks/)
