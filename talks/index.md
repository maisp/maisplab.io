---
layout: page
---

## Can we trust AI to secure our edge?
**Speaker: [Paul Patras](/speakers/)** \\
**University of Edinburgh | Net AI**

People in developed countries have access to almost 8 Internet-connected devices on average. Yet security vulnerabilities continue to threaten the operation of such devices and the privacy of their users, with cybercrime expected to cause damages of 6 trillion US dollars in 2021. Advances in ML/AI are strengthening threat detection, but existing solutions remain sensitive to subtle feature changes, as network traffic evolves. Further, adversarial manipulations can compromise the detection of illicit flows and neural model complexity often hinders their practical deployment at the edge. In this talk I will present a new approach to malicious traffic classification, which relies on low-dimensional embeddings learned with a lightweight neural model comprising multiple kernel networks, and demonstrate this is highly-effective against attacks observed over a span of 20 years. I will then introduce a framework for assessing the robustness of deep learning-based intrusion detection systems against evasion techniques and outline a set of defenses that can restore their effectiveness.

---

## Defence Against Dark Artefacts
**Speaker: [Hamed Haddadi](/speakers/)** \\
**Imperial College London | Brave Software**

Consumer Internet of Things devices often come with a range of sensors and actuators, require access to a variety of personal data sources and continuous internet connectivity, and are equipped with a variety of embedded pre-trained Machine Learning (ML) models. In this talk, I will present our recent findings on privacy threats from these devices and potential mitigation strategies using selective blocking of device activities and destinations. I will then discuss the ways in which we can leverage novel architectures to provide private, trusted, personalised, and dynamically-configurable models on consumer devices to cater for heterogeneous environments and user requirements.

---

## Decentralized Privacy Preserving Artificial Intelligence for Next Generation Telco Industry
**Speaker: [Diego Perino](/speakers/)** \\
**Telefonica Research**

Artificial Intelligence (AI) is recognized as a main driver for future telco industry business and digital transformation. It will lead towards increasingly autonomous and intelligent networks, will improve customer experience, and generate significant economic impact. However, Telco networks are highly complex, distributed and composed of very different , e.g., home environment, IoT devices, mobile phones, Mobile and Fixed Network Operators, Platforms. On the one hand, AI can significantly help in the design and management of such complex ecosystem. On the other hand, the usage of AI can raise privacy and security concerns. In this talk we will briefly describe our experience in research and deployment of AI-based techniques in a large technology company. We will then describe our approach using Federated Learning and Privacy Preserving Mechanisms to deal with emerging privacy concerns while enabling the full benefits of the usage of AI models.


---


## The ML data center is dead: What comes next?
**Speaker: [Nicholas Lane](/speakers/)** \\
**University of Cambridge | Samsung AI Center**

The vast majority of machine learning (ML) occurs today in a data center. But there is a very real possibility that in the (near?) future, we will view this situation similarly to how we now view lead paint, fossil fuels and asbestos: a technological means to an end, that was used for a time because, at that stage, we did not have viable alternatives -- and we did not fully appreciate the negative externalities that were being caused.
Awareness of the unwanted side effects of the current ML data center centric paradigm is building. It couples to ML an alarming carbon footprint, a reliance to biased close-world datasets, serious risks to user privacy -- and promotes centralized control by large organizations due to the assumed extreme compute resources. In this talk, I will sketch some thoughts regarding how a data center free future for ML might come about, and how some of our recent research results (e.g., http://flower.dev) might offer a foundation along this path.